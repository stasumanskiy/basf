

$(document).ready(function() {

  const DOC = $(document);

  // Validation form
  function validation() {
    let validFun = {
      texts: function(val, that, error) {
        if (val.length > 1) {
          that.parent().removeClass(error);
        } else {
          that.parent().addClass(error);
        }
      },
      numbers: function(val, that, error) {
        if (val !== '') {
          that.parent().removeClass(error);
        } else {
          that.parent().addClass(error);
        }
      },
      email: function(val, that, error) {
        let patternEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

        if (patternEmail.test(val)) {
          that.parent().removeClass(error);
        } else {
          that.parent().addClass(error);
        }
      },
      tel: function(val, that, error) {
        let patternTel = /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g;

        if (patternTel.test(val)) {
          that.parent().removeClass(error);
        } else {
          that.parent().addClass(error);
        }
      },
      textarea: function(val, that, error) {
        if (val !== '') {
          that.parent().removeClass(error);
        } else {
          that.parent().addClass(error);
        }
      },
      file: function(val, that, error) {
        if (val !== '') {
          that.parent().removeClass(error);
        } else {
          that.parent().addClass(error);
        }
      },
      password: function(val, that, error) {
        if (val.length >= 7) {
          that.parent().removeClass(error);
        } else {
          that.parent().addClass(error);
        }
      },
      passwordRepeat: function(that, inputValPass, error, val) {
        if (val === inputValPass) {
          that.parent().removeClass(error);
        } else {
          that.parent().addClass(error);
        }
      },
      checked: function(that, inputValPass, error) {
        if (inputValPass === true) {
          that.parent().removeClass(error);
        } else {
          that.parent().addClass(error);
        }
      }
    };

    let checkInputFunction = function() {
      let $that = $(this);
      let $inputAttr = $that.attr('type');
      let $inputVal = $that.val();
      let $required = $that.prop('required');
      let error = 'error';

      if (($required === true) && ($inputAttr === 'text')) {
        validFun.texts($inputVal, $that, error);
      }
      if (($required === true) && ($inputAttr === 'number')) {
        validFun.numbers($inputVal, $that, error);
      }
      if ($inputAttr === 'email') {
        validFun.email($inputVal, $that, error);
      }
      if ($inputAttr === 'tel') {
        validFun.tel($inputVal, $that, error);
      }
      if (($that.hasClass('js-validPass')) && ($inputAttr === 'password')) {
        validFun.password($inputVal, $that, error);
      }
      if (($that.hasClass('js-validPassRepeat')) && ($inputAttr === 'password')) {
        let $inputValPass = $that.parents('.js-validForm');
        let $inputValPassParents = $inputValPass.find('.js-validPass').val();

        validFun.passwordRepeat($that, $inputValPassParents, error, $inputVal);
      }
      if (($required === true) && ($inputAttr === 'checkbox')) {
        let $inputChecked = $that.prop('checked');

        validFun.checked($that, $inputChecked, error);
      }
      if (($required === true) && ($that.hasClass('js-validInputTextarea'))) {
        validFun.textarea($inputVal, $that, error);
      }
    };

    let $addClass = $('.js-validInput');

    $addClass.on('keyup change paste', checkInputFunction);


    let $btnRegistSubmit = $('.js-submitBtn');
    let $customFormValid = $('.js-validForm');

    $btnRegistSubmit.on('click', function() {
      let $customFormValidParent = $(this).parents('.js-validForm');

      showSuccess();
      $customFormValidParent.find('.js-validInput').each(function() {
        let $that = $(this);
        let $required = $that.prop('required');

        if (($required === true) && (($that.val() === '') || ($that.hasClass('error')))) {
          $btnRegistSubmit.prop('type', 'button');
          $that.parent().addClass('error');
          $that.parents('.js-validForm').addClass('error-form');
        }
        if (($required === true) && ($that.attr('type') === 'checkbox') && ($that.prop('checked') === false)) {
          $btnRegistSubmit.prop('type', 'button');
          $that.parent().addClass('error');
        }
        if (($required === true) && ($that.val() === '') && ($that.attr('type') === 'file') && ($that.val() === '')) {
          $btnRegistSubmit.prop('type', 'button');
          $that.parent().addClass('error');
        }
        if (($required === true) && ($that.hasClass('js-validInputTextarea')) && ($that.val() === '')) {
          $btnRegistSubmit.prop('type', 'button');
          $that.parent().addClass('error');
        }
      });
    });

    $customFormValid.on('change', function() {
      $(this).find('.js-validInput').each(function() {
        let $that = $(this);

        if ($that.parent().hasClass('error')) {
          $btnRegistSubmit.prop('type', 'button');
          $that.parents('.js-validForm').addClass('error-form');
        } else {
          $btnRegistSubmit.prop('type', 'submit');
          $that.parents('.js-validForm').removeClass('error-form');
        }
      });
    });
  }

  validation();

  // Init Owl-carousel
  const sliderWorks = $('.js-slider-workspace');

    sliderWorks.each(function() {

    let that = $(this);
    let bigimage = that.find('.js-slider-for');
    let thumbs = that.find('[data-owlNav]');
    let syncedSecondary = true;

    bigimage.owlCarousel({
          items: 1,
          slideSpeed: 2000,
          nav: false,
          autoplay: false,
          dots: false,
          loop: true,
          responsiveRefreshRate: 200
        }).on('changed.owl.carousel', syncPosition);

    thumbs.on('initialized.owl.carousel', function() {
          thumbs.find('.owl-item').eq(0).addClass('current');
        }).owlCarousel({
          items: 3,
          dots: false,
          nav: false,
          smartSpeed: 200,
          slideSpeed: 500,
          slideBy: 1,
          responsiveRefreshRate: 100
        }).on('changed.owl.carousel', syncPosition2);

    function syncPosition(el) {
      var count = el.item.count - 1;
      var current = Math.round(el.item.index - el.item.count / 2 - 0.5);

      if (current < 0) {
        current = count;
      }
      if (current > count) {
        current = 0;
      }

      thumbs.find('.owl-item').removeClass('current').eq(current).addClass('current');
      var onscreen = thumbs.find('.owl-item.active').length - 1;
      var start = thumbs.find('.owl-item.active').first().index();
      var end = thumbs.find('.owl-item.active').last().index();

      if (current > end) {
        thumbs.data('owl.carousel').to(current, 100, true);
      }
      if (current < start) {
        thumbs.data('owl.carousel').to(current - onscreen, 100, true);
      }
    }

    function syncPosition2(el) {
      if (syncedSecondary) {
        var number = el.item.index;
        bigimage.data('owl.carousel').to(number, 100, true);
      }
    }

    thumbs.on('click', '.owl-item', function(e) {
      e.preventDefault();
      var number = $(this).index();
      bigimage.data('owl.carousel').to(number, 300, true);
    });
  });

  // Fancybox init
  const fancyboxWrap = $('.js-fancybox');

  fancyboxWrap.fancybox({
    protect: true,
    keyboard: true,
    animationEffect: false,
    arrows: true,
    clickContent: false,
    loop: true
  });

  // Success show popup
  function showSuccess() {
    let $popupSuccessBlock = $('.js-popupSuccess');

    $('.js-validForm')[0].reset();
    $popupSuccessBlock.fadeIn(100);
  }
});
