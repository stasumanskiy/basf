import $ from 'jquery';
import slick from 'slick-carousel';
import fancybox from '@fancyapps/fancybox';

$(document).ready(function() {

  const DOC = $(document);

  // Validation form

  function validation() {
    let validFun = {
      texts: function(val, that, error) {
        if (val.length > 1) {
          that.parent().removeClass(error);
        } else {
          that.parent().addClass(error);
        }
      },
      numbers: function(val, that, error) {
        if (val !== '') {
          that.parent().removeClass(error);
        } else {
          that.parent().addClass(error);
        }
      },
      email: function(val, that, error) {
        let patternEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

        if (patternEmail.test(val)) {
          that.parent().removeClass(error);
        } else {
          that.parent().addClass(error);
        }
      },
      tel: function(val, that, error) {
        let patternTel = /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g;

        if (patternTel.test(val)) {
          that.parent().removeClass(error);
        } else {
          that.parent().addClass(error);
        }
      },
      textarea: function(val, that, error) {
        if (val !== '') {
          that.parent().removeClass(error);
        } else {
          that.parent().addClass(error);
        }
      },
      file: function(val, that, error) {
        if (val !== '') {
          that.parent().removeClass(error);
        } else {
          that.parent().addClass(error);
        }
      },
      password: function(val, that, error) {
        if (val.length >= 7) {
          that.parent().removeClass(error);
        } else {
          that.parent().addClass(error);
        }
      },
      passwordRepeat: function(that, inputValPass, error, val) {
        if (val === inputValPass) {
          that.parent().removeClass(error);
        } else {
          that.parent().addClass(error);
        }
      },
      checked: function(that, inputValPass, error) {
        if (inputValPass === true) {
          that.parent().removeClass(error);
        } else {
          that.parent().addClass(error);
        }
      }
    };

    let checkInputFunction = function() {
      let $that = $(this);
      let $inputAttr = $that.attr('type');
      let $inputVal = $that.val();
      let $required = $that.prop('required');
      let error = 'error';

      if (($required === true) && ($inputAttr === 'text')) {
        validFun.texts($inputVal, $that, error);
      }
      if (($required === true) && ($inputAttr === 'number')) {
        validFun.numbers($inputVal, $that, error);
      }
      if ($inputAttr === 'email') {
        validFun.email($inputVal, $that, error);
      }
      if ($inputAttr === 'tel') {
        validFun.tel($inputVal, $that, error);
      }
      if (($that.hasClass('js-validPass')) && ($inputAttr === 'password')) {
        validFun.password($inputVal, $that, error);
      }
      if (($that.hasClass('js-validPassRepeat')) && ($inputAttr === 'password')) {
        let $inputValPass = $that.parents('.js-validForm');
        let $inputValPassParents = $inputValPass.find('.js-validPass').val();

        validFun.passwordRepeat($that, $inputValPassParents, error, $inputVal);
      }
      if (($required === true) && ($inputAttr === 'checkbox')) {
        let $inputChecked = $that.prop('checked');

        validFun.checked($that, $inputChecked, error);
      }
      if (($required === true) && ($that.hasClass('js-validInputTextarea'))) {
        validFun.textarea($inputVal, $that, error);
      }
    };

    let $addClass = $('.js-validInput');

    $addClass.on('keyup change paste', checkInputFunction);


    let $btnRegistSubmit = $('.js-submitBtn');
    let $customFormValid = $('.js-validForm');

    $btnRegistSubmit.on('click', function() {
      let $customFormValidParent = $(this).parents('.js-validForm');

      showSuccess();
      $customFormValidParent.find('.js-validInput').each(function() {
        let $that = $(this);
        let $required = $that.prop('required');

        if (($required === true) && (($that.val() === '') || ($that.hasClass('error')))) {
          $btnRegistSubmit.prop('type', 'button');
          $that.parent().addClass('error');
          $that.parents('.js-validForm').addClass('error-form');
        }
        if (($required === true) && ($that.attr('type') === 'checkbox') && ($that.prop('checked') === false)) {
          $btnRegistSubmit.prop('type', 'button');
          $that.parent().addClass('error');
        }
        if (($required === true) && ($that.val() === '') && ($that.attr('type') === 'file') && ($that.val() === '')) {
          $btnRegistSubmit.prop('type', 'button');
          $that.parent().addClass('error');
        }
        if (($required === true) && ($that.hasClass('js-validInputTextarea')) && ($that.val() === '')) {
          $btnRegistSubmit.prop('type', 'button');
          $that.parent().addClass('error');
        }
      });
    });

    $customFormValid.on('change', function() {
      $(this).find('.js-validInput').each(function() {
        let $that = $(this);

        if ($that.parent().hasClass('error')) {
          $btnRegistSubmit.prop('type', 'button');
          $that.parents('.js-validForm').addClass('error-form');
        } else {
          $btnRegistSubmit.prop('type', 'submit');
          $that.parents('.js-validForm').removeClass('error-form');
        }
      });
    });
  }

  validation();

  // DOC.ready(() => {
  //
  //   const sliderWorks = $('.js-slider-workspace');
  //
  //   sliderWorks.each((i, el) => {
  //
  //     let that = $(el);
  //     let sliderFor = that.find('.js-slider-for');
  //     let sliderNav = that.find('.js-slider-nav');
  //
  //     sliderFor.slick({
  //       slidesToShow: 1,
  //       slidesToScroll: 1,
  //       arrows: false,
  //       fade: true,
  //       adaptiveHeight: true,
  //       asNavFor: sliderNav
  //     });
  //     sliderNav.slick({
  //       slidesToShow: 3,
  //       slidesToScroll: 1,
  //       asNavFor: sliderFor,
  //       dots: false,
  //       arrows: false,
  //       focusOnSelect: true,
  //       responsive: [
  //         {
  //           breakpoint: 768,
  //           settings: {
  //             vertical: false,
  //           }
  //         }
  //       ]
  //     });
  //   });
  // });

  //    Fancybox init
  const fancyboxWrap = $('.js-fancybox');

  fancyboxWrap.fancybox({
    protect: true,
    keyboard: true,
    animationEffect: false,
    arrows: true,
    clickContent: false
  });

  // Success show popup

  function showSuccess() {
    let $popupSuccessBlock = $('.js-popupSuccess');

    $('.js-validForm')[0].reset();
    $popupSuccessBlock.fadeIn(100);
  }
});
